using System;
using System.Collections.Generic;

namespace cscsg
{

    class TextCommand : Command
    {

        public readonly string text;
        public Func<string, bool> callback;

        public string getName()
        {
            return text;
        }

        public override bool match(string input)
        {
            if (input == null) return false;
            if (input.Trim().Split(' ')[0] == this.text)
            {
                return this.callback.Invoke(input);
            }
            return false;
        }

        public TextCommand(string text)
        {
            this.text = text;
        }

        public TextCommand(string text, Func<string, bool> callback)
        {
            this.text = text;
            this.callback = callback;
        }



        public static List<string> commandNames = new List<string>();

        private static void addCmd(TextCommand cmd)
        {
            allCommands.Add(cmd);
            commandNames.Add(cmd.getName());
        }

        public static new void init()
        {
            World world = Game.world;

            addCmd(new TextCommand("w", (cmd) =>
            {
                return Command.move(world, Direction.NORTH);
            }));
            addCmd(new TextCommand("s", (cmd) =>
            {
                return Command.move(world, Direction.SOUTH);
            }));
            addCmd(new TextCommand("a", (cmd) =>
            {
                return Command.move(world, Direction.WEST);
            }));
            addCmd(new TextCommand("d", (cmd) =>
            {
                return Command.move(world, Direction.EAST);
            }));
            addCmd(new TextCommand("W", (cmd) =>
            {
                return Command.move(world, Direction.NORTH, true);
            }));
            addCmd(new TextCommand("S", (cmd) =>
            {
                return Command.move(world, Direction.SOUTH, true);
            }));
            addCmd(new TextCommand("A", (cmd) =>
            {
                return Command.move(world, Direction.WEST, true);
            }));
            addCmd(new TextCommand("D", (cmd) =>
            {
                return Command.move(world, Direction.EAST, true);
            }));

            addCmd(new TextCommand("i", (cmd) =>
            {
                return Command.moveSelection(Direction.NORTH);
            }));
            addCmd(new TextCommand("k", (cmd) =>
            {
                return Command.moveSelection(Direction.SOUTH);
            }));
            addCmd(new TextCommand("j", (cmd) =>
            {
                return Command.moveSelection(Direction.WEST);
            }));
            addCmd(new TextCommand("l", (cmd) =>
            {
                return Command.moveSelection(Direction.EAST);
            }));
            addCmd(new TextCommand("I", (cmd) =>
            {
                return Command.moveSelection(Direction.NORTH, true);
            }));
            addCmd(new TextCommand("K", (cmd) =>
            {
                return Command.moveSelection(Direction.SOUTH, true);
            }));
            addCmd(new TextCommand("J", (cmd) =>
            {
                return Command.moveSelection(Direction.WEST, true);
            }));
            addCmd(new TextCommand("L", (cmd) =>
            {
                return Command.moveSelection(Direction.EAST, true);
            }));
            addCmd(new TextCommand("-", (cmd) =>
            {
                return Command.moveSelection(Direction.DOWN);
            }));
            addCmd(new TextCommand("=", (cmd) =>
            {
                return Command.moveSelection(Direction.UP);
            }));
            addCmd(new TextCommand("[", (cmd) =>
            {
                return Command.selectEntity(-1);
            }));
            addCmd(new TextCommand("]", (cmd) =>
            {
                return Command.selectEntity(1);
            }));

            addCmd(new TextCommand("r", (cmd) =>
            {
                return Command.rest();
            }));
            addCmd(new TextCommand("R", (cmd) =>
            {
                return Command.rest(true);
            }));

            addCmd(new TextCommand("q", (cmd) =>
            {
                return Command.mine(world);
            }));
            addCmd(new TextCommand("mine", (cmd) =>
            {
                return Command.mine(world);
            }));
            addCmd(new TextCommand("f", (cmd) =>
            {
                return Command.placeOrUse(world);
            }));
            addCmd(new TextCommand("place", (cmd) =>
            {
                return Command.place(world);
            }));
            addCmd(new TextCommand("use", (cmd) =>
            {
                Command.use(world);
                return true;
            }));
            addCmd(new TextCommand("e", (cmd) =>
            {
                return Command.interact(world);
            }));
            addCmd(new TextCommand("interact", (cmd) =>
            {
                return Command.interact(world);
            }));



            addCmd(new TextCommand("p", (cmd) =>
            {
                Game.printLn(new TranslatableText("command.mode.key.switch"));
                Game.state.mode = Mode.KEY;
                return true;
            }));

            addCmd(new TextCommand("help", (cmd) =>
            {
                Game.openScreen(new HelpScreen(Mode.TEXT));
                return true;
            }));
            addCmd(new TextCommand("colors", (cmd) =>
            {
                Game.openScreen(new ColorScreen());
                return true;
            }));
            addCmd(new TextCommand("tiles", (cmd) =>
            {
                Game.openScreen(new TileScreen());
                return true;
            }));
            addCmd(new TextCommand("items", (cmd) =>
            {
                Game.openScreen(new ItemScreen());
                return true;
            }));
            addCmd(new TextCommand("rt", (cmd) =>
            {
                Translation.reload();
                return true;
            }));
            addCmd(new TextCommand("info", (cmd) =>
            {
                Game.state.extraInfo = !Game.state.extraInfo;
                return true;
            }));
            addCmd(new TextCommand("inv", (cmd) =>
            {
                Game.openScreen(new InventoryScreen());
                return true;
            }));
            addCmd(new TextCommand("pos", (cmd) =>
            {
                Command.printPlayerPos();
                Command.printSelPos();
                return true;
            }));
            addCmd(new TextCommand("save", (cmd) =>
            {
                world.saveAll();
                return true;
            }));
            addCmd(new TextCommand("saveas", (cmd) =>
            {
                Console.WriteLine($"{Formatting.RESET_STR}Type world name:");
                string newName = Console.ReadLine();
                if (newName != null && newName != "") {
                    world.filename = newName;
                }
                world.saveAll();
                return true;
            }));
            addCmd(new TextCommand("seed", (cmd) =>
            {
                Game.printLn(new TranslatableText("info.seed", world.getSeed().ToString()));
                return true;
            }));
            addCmd(new TextCommand("settings", (cmd) =>
            {
                Game.openScreen(new SettingsScreen());
                return true;
            }));
            addCmd(new TextCommand("exit", (cmd) =>
            {
                Game.stop();
                return true;
            }));




            addCmd(new TextCommand("debug", (cmd) =>
            {
                return Command.toggleDebug();
            }));
            addCmd(new TextCommand("chkdbg", (cmd) =>
            {
                if (Game.state.debugMode)
                {
                    Game.openScreen(new ChunkDebugScreen());
                    return true;
                }
                Game.error(new TranslatableText("command.error.debugDisabled"));
                return true;
            }));
            addCmd(new TextCommand("testd", (cmd) =>
            {
                if (Game.state.debugMode)
                {
                    DialogueManager.startDialogue(new Dialogue(new TranslatableText("dialogue.test")));
                }
                Game.error(new TranslatableText("command.error.debugDisabled"));
                return true;
            }));
            addCmd(new TextCommand("give", (cmd) =>
            {
                if (Game.state.debugMode)
                {
                    Game.player.give(world, new ItemStack(Item.STICK, 17));
                    return true;
                }
                Game.error(new TranslatableText("command.error.debugDisabled"));
                return true;
            }));
            addCmd(new TextCommand("givelots", (cmd) =>
            {
                if (Game.state.debugMode)
                {
                    Game.player.give(world, new ItemStack(Item.STICK, 1913));
                    return true;
                }
                Game.error(new TranslatableText("command.error.debugDisabled"));
                return true;
            }));
            addCmd(new TextCommand("take", (cmd) =>
            {
                if (Game.state.debugMode)
                {
                    Game.player.inventory.removeItem(Item.STICK.getId(), 10);
                    return true;
                }
                Game.error(new TranslatableText("command.error.debugDisabled"));
                return true;
            }));
            addCmd(new TextCommand("spawn", (cmd) =>
            {
                if (Game.state.debugMode)
                {
                    Pig pig = new Pig();
                    pig.pos = new Vector3<int>(Game.player.pos);
                    world.addEntity(pig);
                    return true;
                }
                Game.error(new TranslatableText("command.error.debugDisabled"));
                return true;
            }));

        }

    }

}