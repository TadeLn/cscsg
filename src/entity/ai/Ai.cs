namespace cscsg
{
    abstract class Ai
    {
        public abstract void tick(Entity entity, World world);
    }
}