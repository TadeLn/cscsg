using System.Text.Json.Nodes;

namespace cscsg
{
    class Zombie : Monster
    {
        public override string getNameTKey()
        {
            return "entity.zombie.name";
        }

        public override TileChar getTileChar()
        {
            return new TileChar(new Char("🧟", "☥", "!"), Formatting.GREEN);
        }

        public Zombie() : base(Entity.ZOMBIE, new AttackingAi(20))
        {
            this.maxHealth = 50;
            this.health = maxHealth;
        }

        

        public static new Entity fromJson(JsonObject j)
        {
            Zombie result = new Zombie();
            result.loadJson(j);
            return result;
        }
    }
}
