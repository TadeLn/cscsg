namespace cscsg
{
    class Monster : NPC
    {
        public override MobcapType getMobcapType()
        {
            return MobcapType.MONSTER;
        }

        public Monster(byte typeId, Ai ai) : base(typeId, ai)
        {
        }
    }
}