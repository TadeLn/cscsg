using System;
using System.IO;
using System.IO.Compression;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace cscsg
{
    static class JsonLoader
    {
        // Load json from file (throws exceptions on error)
        public static JsonNode loadFileE(string path)
        {
            JsonNode node = null;
            string json = File.ReadAllText(path);
            node = JsonNode.Parse(json);
            return node;
        }

        // Load json from file (returns null on error)
        public static JsonNode loadFile(string path)
        {
            try
            {
                return loadFileE(path);
            }
            catch (Exception)
            {
                return null;
            }
        }

        // Load json from compressed file (throws exceptions on error)
        public static JsonNode loadCompressedFileE(string path)
        {
            JsonNode node = null;
            FileStream fs = File.OpenRead(path);
            GZipStream gzip = new GZipStream(fs, CompressionMode.Decompress);
            StreamReader reader = new StreamReader(gzip);
            string json = reader.ReadToEnd();
            reader.Close();
            gzip.Close();
            fs.Close();

            node = JsonNode.Parse(json);
            return node;
        }

        // Load json from compressed file (returns null on error)
        public static JsonNode loadCompressedFile(string path)
        {
            try
            {
                return loadCompressedFileE(path);
            }
            catch (Exception)
            {
                return null;
            }
        }

        // Save json to file
        public static void saveFile(string path, JsonNode jsonNode)
        {
            JsonSerializerOptions options = new JsonSerializerOptions { WriteIndented = true };
            File.WriteAllText(path, jsonNode.ToJsonString(options));
        }

        // Save json to compressed file
        public static void saveCompressedFile(string path, JsonNode jsonNode)
        {
            JsonSerializerOptions options = new JsonSerializerOptions { WriteIndented = true };
            FileStream fs = File.OpenWrite(path);
            GZipStream gzip = new GZipStream(fs, CompressionMode.Compress);
            StreamWriter writer = new StreamWriter(gzip);
            writer.Write(jsonNode.ToJsonString(options));
            writer.Flush();
            writer.Close();
            gzip.Close();
            fs.Close();
        }
    }
}