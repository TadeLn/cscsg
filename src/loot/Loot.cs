using System;
using System.Collections.Generic;

namespace cscsg
{
    class Loot
    {
        public List<LootEntry> entries = new List<LootEntry>();
        public int rolls = 1;

        public List<ItemStack> get(int count, Random random)
        {
            List<ItemStack> result = new List<ItemStack>();

            int weightSum_ = 0;
            foreach (LootEntry entry in this.entries) {
                weightSum_ += entry.weight;
            }

            for (int i = 0; i < count; i++)
            {
                List<LootEntry> entries = new List<LootEntry>(this.entries);
                int weightSum = weightSum_;

                // Get `rolls` of entries
                // Select entry at random, weighted by entry.weight
                // Add their result to the result
                for (int j = 0; j < rolls; j++) {
                    int r = random.Next(weightSum);

                    int k = 0;
                    LootEntry entry = entries[k];
                    while (r > entry.weight) {
                        r -= entry.weight;
                        k++;
                        entry = entries[k];
                    }

                    result.Add(entry.get(random));

                    weightSum -= entry.weight;
                    entries.RemoveAt(k);
                }
            }
            return result;
        }


        public Loot addEntry(LootEntry entry) {
            this.entries.Add(entry);
            return this;
        }

        public Loot setRolls(int rolls) {
            this.rolls = rolls;
            return this;
        }


        public Loot(int rolls = 1)
        {
            this.rolls = rolls;
        }
    }
}