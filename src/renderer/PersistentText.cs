namespace cscsg
{
    class PersistentText
    {
        public int ticks;
        public string text;

        // Returns true if it needs to be deleted
        public bool tick()
        {
            ticks--;
            return ticks < 0;
        }

        public PersistentText(string text = "", int ticks = 1)
        {
            this.text = text;
            this.ticks = ticks;
        }
    }
}