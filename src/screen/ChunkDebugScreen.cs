using System;
using System.Text;
using System.Collections.Generic;

namespace cscsg
{
    class ChunkDebugScreen : Screen
    {
        public const int LINES = 3;

        public Vector2<int> selection = World.getChunkPos(Game.player.pos.x, Game.player.pos.z);

        public override void render()
        {
            World world = Game.world;
            Settings s = Game.settings;

            Console.Clear();
            Console.Write($"{Formatting.REVERSED}");
            Console.Write(Util.center(new TranslatableText("screen.chunkDebug.title").getUnformatted(), s.getScreenWidth()));
            Console.WriteLine($"{Formatting.RESET_STR}");

            int scrollX = selection.x - (Game.settings.getScreenWidth() / (Game.settings.textDoubleWidth ? 4 : 2));
            int scrollz = selection.y - (Game.settings.getScreenHeight() / 2);

            Inventory inv = Game.player.inventory;

            StringBuilder sb = new StringBuilder();

            Vector2<int> playerChunkPos = World.getChunkPos(Game.player.pos.x, Game.player.pos.z);

            for (int z = scrollz; z < scrollz + Game.settings.getScreenHeight() - LINES; z++)
            {
                int xIterations = Game.settings.getScreenWidth() / (Game.settings.textDoubleWidth ? 2 : 1);
                for (int x = scrollX; x < scrollX + xIterations; x++)
                {
                    char ch = ' ';
                    Chunk c = world.getChunk(x, z);

                    if (c == null)
                    {
                        ch = '.';
                    }
                    else
                    {
                        if (c.isPopulated)
                        {
                            ch = 'O';
                        }
                        else
                        {
                            ch = '-';
                        }
                    }

                    if (world.getChunksToUnload().Contains(World.toCoordsId(x, z)))
                    {
                        ch = '*';
                    }
                    if (x == 0 && z == 0)
                    {
                        ch = '+';
                    }
                    if (x == playerChunkPos.x && z == playerChunkPos.y)
                    {
                        ch = '@';
                    }

                    sb.Append(ch);
                    if (Game.settings.textDoubleWidth)
                    {
                        sb.Append(ch);
                    }
                }
                sb.AppendLine();
            }
            Console.Write(sb.ToString());
            Console.Write($"{Formatting.REVERSED}");
            Console.Write(Util.center(new TranslatableText("screen.chunkDebug.hint").getUnformatted(), s.getScreenWidth()));
            Console.WriteLine($"{Formatting.RESET_STR}");
        }

        public override void input()
        {
            Inventory inv = Game.player.inventory;
            try
            {
                ConsoleKeyInfo key = Console.ReadKey();
                Game.state.keyModeAvaliable = true;
                switch (key.Key)
                {
                    case ConsoleKey.UpArrow:
                        selection.y--;
                        break;

                    case ConsoleKey.DownArrow:
                        selection.y++;
                        break;

                    case ConsoleKey.LeftArrow:
                        selection.x--;
                        break;

                    case ConsoleKey.RightArrow:
                        selection.x++;
                        break;

                    case ConsoleKey.Escape:
                        Game.closeScreen();
                        break;
                }
            }
            catch (System.InvalidOperationException)
            {
                Game.state.keyModeAvaliable = false;
                string line = Console.ReadLine();
                if (line == "w") selection.y--;
                if (line == "s") selection.y++;
                if (line == "a") selection.x--;
                if (line == "d") selection.x++;
                if (line == "q" || line == "quit") Game.closeScreen();
            }
        }
    }
}