using System;

namespace cscsg
{
    class DialogueScreen : Screen
    {
        Dialogue dialogue;

        public DialogueScreen(Dialogue dialogue) {
            this.dialogue = dialogue;
        }

        public override void render() {
            Console.Clear();
            bool skip = false;
            DialogueManager.printSlow(dialogue.getString(), ref skip);
            DialogueManager.printSlowRaw($"\n\n", ref skip, 2);
            DialogueManager.printSlowRaw(new TranslatableText("dialogue.option.continue").format(Formatting.REVERSED), ref skip, 2);

            try
            {
                Console.ReadKey();
            }
            catch (System.InvalidOperationException)
            {
                Console.ReadLine();
            }
        }

        public override void input() {
            Game.closeScreen();
        }
    }
}