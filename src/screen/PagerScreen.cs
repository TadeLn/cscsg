using System;
using System.Collections.Generic;

namespace cscsg
{
    abstract class PagerScreen : Screen
    {
        public const int LINES = 3;

        protected int scroll = 0;
        protected FormattedText title;

        public abstract List<Text> getLines();

        public virtual string getHintTKey()
        {
            return Game.state.keyModeAvaliable ? "screen.pager.hint.key" : "screen.pager.hint.text";
        }

        public int getLineCount()
        {
            Settings s = Game.settings;
            int screenWidth = s.getScreenWidth();
            int screenHeight = s.getScreenHeight() - LINES;

            List<Text> lines = this.getLines();
            int lineNum = 0;
            foreach (Text it in lines)
            {
                string line = it.get();

                int subline = 0;
                while (line.Length > screenWidth * (subline + 1))
                {
                    subline++;
                    lineNum++;
                }
                lineNum++;
            }
            return lineNum;
        }

        public virtual void updateScroll()
        {
            List<Text> lines = getLines();
            int lineCount = getLineCount();
            int screenHeight = Game.settings.getScreenHeight() - LINES;
            if (scroll > lineCount - screenHeight)
            {
                scroll = lineCount - screenHeight;
            }
            if (scroll < 0)
            {
                scroll = 0;
            }
        }

        public virtual void handleKey(ConsoleKeyInfo key)
        {
            switch (key.Key)
            {
                case ConsoleKey.UpArrow:
                    scroll--;
                    break;

                case ConsoleKey.PageUp:
                    scroll -= Game.settings.getScreenHeight() - LINES;
                    break;

                case ConsoleKey.Home:
                    scroll = 0;
                    break;

                case ConsoleKey.DownArrow:
                    scroll++;
                    break;

                case ConsoleKey.PageDown:
                    scroll += Game.settings.getScreenHeight() - LINES;
                    break;

                case ConsoleKey.End:
                    scroll = int.MaxValue;
                    break;

                case ConsoleKey.Escape:
                    Game.closeScreen();
                    break;
            }

        }

        public virtual void handleText(string text)
        {
            if (text == "u") scroll--;
            if (text == "d") scroll++;
            if (text == "q" || text == "quit") Game.closeScreen();
        }


        public override void render()
        {
            Settings s = Game.settings;
            Console.Clear();
            Console.Write($"{Formatting.REVERSED}");
            Console.Write(Util.center(title.getUnformatted(), s.getScreenWidth()));
            Console.WriteLine($"{Formatting.RESET_STR}");

            int screenWidth = s.getScreenWidth();
            int screenHeight = s.getScreenHeight() - LINES;
            int skip = scroll;

            List<Text> lines = this.getLines();
            int lineNum = 0;
            foreach (Text it in lines)
            {
                string line = it.get();

                int subline = 0;
                while (line.Length > screenWidth * (subline + 1))
                {
                    if (skip > 0)
                    {
                        skip--;
                        subline++;
                        lineNum++;
                    }
                    else
                    {
                        Console.WriteLine(line.Substring(subline * screenWidth, screenWidth));
                        subline++;
                        lineNum++;
                        if (lineNum >= scroll + screenHeight) goto end;
                    }
                }
                if (skip > 0)
                {
                    skip--;
                    lineNum++;
                }
                else
                {
                    Console.WriteLine(line.Substring(subline * screenWidth, line.Length - (subline * screenWidth)));
                    lineNum++;
                    if (lineNum >= scroll + screenHeight) goto end;
                }
            }
        end:
            while (lineNum < scroll + screenHeight)
            {
                Console.WriteLine();
                lineNum++;
            }

            Console.Write($"{Formatting.REVERSED}");
            Console.Write(Util.center(new TranslatableText(this.getHintTKey()).getUnformatted(), s.getScreenWidth()));
            Console.WriteLine($"{Formatting.RESET_STR}");
        }

        public override void input()
        {
            try
            {
                ConsoleKeyInfo key = Console.ReadKey();
                Game.state.keyModeAvaliable = true;
                this.handleKey(key);
            }
            catch (System.InvalidOperationException)
            {
                Game.state.keyModeAvaliable = false;
                string line = Console.ReadLine();
                this.handleText(line);
            }
            updateScroll();
        }

        public PagerScreen(FormattedText title)
        {
            this.title = title;
        }

        public PagerScreen(string title) : this(new TranslatableText(title)) { }

        public PagerScreen() : this(new TranslatableText("screen.undefined.title")) { }
    }
}