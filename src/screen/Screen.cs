namespace cscsg
{
    abstract class Screen
    {
        public Screen parent = null;
        public virtual void open() { }
        public virtual void close() { }
        public abstract void render();
        public abstract void input();
    }
}