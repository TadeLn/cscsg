using System.Collections.Generic;
using System.Text;

namespace cscsg
{
    class TileScreen : PagerScreen
    {

        public List<Text> lines = new List<Text>();
        public override List<Text> getLines() {
            return this.lines;
        }

        public TileScreen() : base("screen.tiles.title"){
            bool doubleChar = Game.settings.textDoubleWidth;
            World world = Game.world;

            for (int i = 0; i < 256; i++) {
                Tile tile = Tile.fromId((byte)i);
                if (tile != null) {
                    TileChar floor = tile.getFloorChar(world, 0, 0, 0);
                    TileChar wall = tile.getWallChar(world, 0, 0, 0);

                    StringBuilder sb = new StringBuilder();
                    sb.Append(new TranslatableText("screen.tiles.tile", i.ToString("000")).get())
                        .Append('\t')
                        .Append(floor.format)
                        .Append(floor.c);
                    if (doubleChar) {
                        sb.Append(floor.c);
                    }
                    sb.Append(Formatting.RESET_STR)
                        .Append('\t')
                        .Append(wall.format)
                        .Append(wall.c);
                    if (doubleChar) {
                        sb.Append(wall.c);
                    }
                    sb.Append(Formatting.RESET_STR)
                        .Append('\t')
                        .Append(Translation.get(tile.getName()));

                    lines.Add(new LiteralText(sb.ToString()));
                }
            }
        }
    }
}