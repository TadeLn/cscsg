using System;
using System.Collections.Generic;

namespace cscsg
{
    class WorldScreen : Screen
    {
        World world = Game.world;

        public override void render()
        {
            Game.renderer.renderNormal(world);
        }



        public void handleCommand(string input)
        {
            foreach (Command c in Command.allCommands)
            {
                if (c.match(input))
                {
                    return;
                }
            }

            string inputName = input.Split(' ')[0].Trim();
            string nearestMatch = Levenshtein.getNearest(TextCommand.commandNames, inputName);

            if (inputName.Length > 1 && Levenshtein.getEditDistance(nearestMatch, inputName) < 10)
            {
                Game.error(new TranslatableText("command.error.unknownCommand.suggestion", nearestMatch));
            }
            else
            {
                Game.error(new TranslatableText("command.error.unknownCommand"));
            }
        }

        public void handleKey(ConsoleKeyInfo key)
        {
            foreach (Command c in Command.allCommands)
            {
                if (c.matchKey(key))
                {
                    return;
                }
            }
            if (Game.state.debugMode) {
                Game.printLn(new TranslatableText("debug.unhandledKey", key.ToString(), key.Key.ToString()));
            }
        }

        public override void input()
        {
            if (Game.state.mode == Mode.KEY)
            {
                try
                {
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    Game.state.keyModeAvaliable = true;
                    handleKey(key);
                }
                catch (System.InvalidOperationException)
                {
                    Game.state.keyModeAvaliable = false;
                    Game.error(new TranslatableText("error.mode.key.unavaliable"));
                    Game.state.mode = Mode.TEXT;
                }
            }
            else if (Game.state.mode == Mode.TEXT)
            {
                string input = Console.ReadLine();
                handleCommand(input);
            }
            else
            {
                Game.error(new TranslatableText("error.mode.invalid"));
                Game.state.mode = Mode.TEXT;
            }
        }
    }
}