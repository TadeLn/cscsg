using System;
using System.Globalization;

namespace cscsg
{
    class FloatSetting : NumberSetting<float>
    {
        public override object get()
        {
            return value;
        }

        public override void set()
        {
            Console.WriteLine("Input number:");
            string line = Console.ReadLine();
            if (line == "")
            {
                if (allowNull)
                {
                    value = null;
                }
            }
            try
            {
                value = Convert.ToSingle(line, NumberFormatInfo.GetInstance(CultureInfo.GetCultureInfo("en")));
                if (value > max)
                {
                    value = max;
                }
                if (value < min)
                {
                    value = min;
                }
            }
            catch (Exception)
            { }
        }

        public override Text getValueText()
        {
            if (this.value.HasValue)
            {
                return new LiteralText(this.value.Value.ToString(NumberFormatInfo.GetInstance(CultureInfo.GetCultureInfo("en"))));
            }
            else
            {
                return new TranslatableText(this.nullTKey);
            }
        }


        public FloatSetting(string nameTKey, float value, float min = float.MinValue, float max = float.MaxValue, bool allowNull = false, string nullTKey = "settings.null")
            : base(nameTKey, value, min, max, allowNull, nullTKey)
        { }

        public FloatSetting(string nameTKey, Nullable<float> value, float min = float.MinValue, float max = float.MaxValue, bool allowNull = false, string nullTKey = "settings.null")
            : base(nameTKey, value, min, max, allowNull, nullTKey)
        { }
    }
}