using System.Text.Json.Nodes;

namespace cscsg
{
    abstract class Setting
    {
        public abstract object get();
        public abstract void set();
        public abstract Text getText();
    }
}