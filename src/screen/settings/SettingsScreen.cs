using System;
using System.Collections.Generic;

namespace cscsg
{
    class SettingsScreen : PagerScreen
    {
        public Dictionary<string, Setting> settings = new Dictionary<string, Setting>();

        int selectedSetting = 0;

        public override List<Text> getLines()
        {
            List<Text> lines = new List<Text>();
            int lineNum = 0;
            foreach (var it in this.settings)
            {
                string line = "";

                if (lineNum == selectedSetting)
                {
                    line += Formatting.REVERSED.getANSI();
                    line += " > ";
                }
                line += it.Value.getText().getUnformatted();
                if (lineNum == selectedSetting)
                {
                    line += " < ";
                    line += Formatting.RESET_STR;
                }

                lines.Add(new LiteralText(line));
                lineNum++;
            }
            return lines;
        }

        public override string getHintTKey()
        {
            return Game.state.keyModeAvaliable ? "screen.settings.hint.key" : "screen.settings.hint.text";
        }

        protected void selectSetting(int delta)
        {
            selectedSetting += delta;
            if (selectedSetting >= settings.Count)
            {
                selectedSetting = settings.Count - 1;
            }
            if (selectedSetting < 0)
            {
                selectedSetting = 0;
            }
            updateScroll();
        }

        protected void setSetting()
        {
            string[] keys = new string[settings.Count];
            settings.Keys.CopyTo(keys, 0);
            settings[keys[selectedSetting]].set();
        }

        protected void applySettings()
        {
            Game.settings.screenWidth = (Nullable<int>)this.settings["screenWidth"].get();
            Game.settings.screenHeight = (Nullable<int>)this.settings["screenHeight"].get();
            Game.settings.lang = (string)this.settings["lang"].get();
            Game.settings.textSpeed = (float)this.settings["textSpeed"].get();
            Game.settings.textColor = (ColorLevel)this.settings["textColor"].get();
            Game.settings.textUnicode = (bool)this.settings["textUnicode"].get();
            Game.settings.textDoubleWidth = (bool)this.settings["textDoubleWidth"].get();
            Game.settings.textEmoji = (bool)this.settings["textEmoji"].get();
            Translation.reload(Game.settings.lang);
            Game.settings.save(Settings.FILENAME);
        }

        protected void loadSettings()
        {
            Settings.load(Settings.FILENAME);
        }

        public override void updateScroll()
        {
            this.scroll = selectedSetting + (Game.settings.getScreenHeight() - LINES) / 2;
            base.updateScroll();
        }

        public override void handleKey(ConsoleKeyInfo key)
        {
            switch (key.Key)
            {
                case ConsoleKey.Escape:
                    Game.closeScreen();
                    break;

                case ConsoleKey.UpArrow:
                    selectSetting(-1);
                    break;

                case ConsoleKey.DownArrow:
                    selectSetting(1);
                    break;

                case ConsoleKey.Enter:
                    setSetting();
                    break;

                case ConsoleKey.A:
                    applySettings();
                    break;

                case ConsoleKey.R:
                    loadSettings();
                    break;
            }
            updateScroll();
        }

        public override void handleText(string text)
        {
            if (text == "q" || text == "quit") Game.closeScreen();
            if (text == "u") selectSetting(-1);
            if (text == "d") selectSetting(1);
            if (text == "s") setSetting();
            if (text == "a") applySettings();
            if (text == "r") loadSettings();
            updateScroll();
        }

        public SettingsScreen() : base("screen.settings.title")
        {
            Settings s = Game.settings;
            settings.Add("screenWidth", new IntSetting("setting.screenWidth", s.screenWidth, min: 1, allowNull: true, nullTKey: "settings.auto"));
            settings.Add("screenHeight", new IntSetting("setting.screenHeight", s.screenHeight, min: 1, allowNull: true, nullTKey: "settings.auto"));
            settings.Add("lang", new LanguageSetting("setting.lang", s.lang));
            settings.Add("textSpeed", new FloatSetting("setting.textSpeed", s.textSpeed, min: 1));
            settings.Add("textColor", new IntSetting("setting.textColor", (int)s.textColor, min: 0, max: 3));
            settings.Add("textUnicode", new BoolSetting("setting.textUnicode", s.textUnicode, trueTKey: "settings.yes", falseTKey: "settings.no"));
            settings.Add("textDoubleWidth", new BoolSetting("setting.textDoubleWidth", s.textDoubleWidth, trueTKey: "settings.yes", falseTKey: "settings.no"));
            settings.Add("textEmoji", new BoolSetting("setting.textEmoji", s.textEmoji, trueTKey: "settings.yes", falseTKey: "settings.no"));
        }
    }
}