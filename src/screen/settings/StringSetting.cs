using System;

namespace cscsg
{
    class StringSetting : NamedSetting
    {
        protected string nullTKey;
        protected string value = null;

        public override object get()
        {
            return value;
        }

        public override void set()
        {
            Console.WriteLine("Input text:");
            string line = Console.ReadLine();
            if (line == "")
            {
                value = null;
            }
            else
            {
                value = line;
            }
        }

        public override Text getValueText()
        {
            if (this.value != null)
            {
                return new LiteralText("\"").append(this.value.ToString()).append("\"");
            }
            else
            {
                return new TranslatableText(this.nullTKey);
            }
        }

        public StringSetting(string nameTKey, string value, string nullTKey = "settings.null") : base(nameTKey)
        {
            this.value = value;
            this.nullTKey = nullTKey;
        }
    }
}