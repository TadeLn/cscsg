using System.Text.Json;
using System.Text.Json.Nodes;
using System.IO;
using System;

namespace cscsg
{
    class Settings
    {
        public const string FILENAME = "settings.json";

        public Nullable<int> screenWidth = null;
        public Nullable<int> screenHeight = null;
        public string lang = "en_us";

        public float textSpeed = 25;
        public ColorLevel textColor = ColorLevel.COLOR_256;
        public bool textUnicode = true;
        public bool textDoubleWidth = true;
        public bool textEmoji = true;



        public int lastScreenWidth = 0;
        public int lastScreenHeight = 0;

        public int getScreenWidth()
        {
            return screenWidth.GetValueOrDefault(lastScreenWidth);
        }

        public int getScreenHeight()
        {
            return screenHeight.GetValueOrDefault(lastScreenHeight);
        }


        public void update()
        {
            this.lastScreenWidth = Console.WindowWidth - 1;
            this.lastScreenHeight = Console.WindowHeight - 1;
        }



        public static Settings load(string filename, bool printErrors = true)
        {
            JsonObject j = null;
            try
            {
                j = JsonLoader.loadFileE(filename).AsObject();
            }
            catch (Exception e)
            {
                if (printErrors)
                {
                    Game.error(new TranslatableText("error.load.settings", e.ToString()));
                }
                return null;
            }

            Settings r = new Settings();
            JsonHelper.load(j, "screenWidth", ref r.screenWidth);
            JsonHelper.load(j, "screenHeight", ref r.screenHeight);
            JsonHelper.load(j, "lang", ref r.lang);
            JsonHelper.load(j, "textSpeed", ref r.textSpeed);

            int textColor = 0;
            JsonHelper.load(j, "textColor", ref textColor);
            r.textColor = (ColorLevel)textColor;

            JsonHelper.load(j, "textUnicode", ref r.textUnicode);
            JsonHelper.load(j, "textDoubleWidth", ref r.textDoubleWidth);
            JsonHelper.load(j, "textEmoji", ref r.textEmoji);
            return r;
        }

        public void save(string filename)
        {
            JsonObject j = new JsonObject();
            j.Add("screenWidth", JsonValue.Create(this.screenWidth));
            j.Add("screenHeight", JsonValue.Create(this.screenHeight));
            j.Add("lang", JsonValue.Create(this.lang));
            j.Add("textSpeed", JsonValue.Create(this.textSpeed));
            j.Add("textColor", JsonValue.Create((int)this.textColor));
            j.Add("textUnicode", JsonValue.Create(this.textUnicode));
            j.Add("textDoubleWidth", JsonValue.Create(this.textDoubleWidth));
            j.Add("textEmoji", JsonValue.Create(this.textEmoji));
            JsonLoader.saveFile(filename, j);
        }


        public Settings()
        {
            update();
        }
    }
}