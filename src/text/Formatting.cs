using System;
using System.Text;

namespace cscsg
{
    class Formatting
    {
        public Color color;
        public Color background;
        public bool bold;
        public bool underline;
        public bool reversed;
        public bool isReset = false;


        public string getANSI()
        {
            if (Game.settings.textColor == 0) return "";

            if (isReset) return Color.RESET.getCode();
            StringBuilder sb = new StringBuilder();
            if (!color.isReset()) sb.Append(color.getCode());
            if (!background.isReset()) sb.Append(background.getBackgroundCode());
            if (bold) sb.Append("\u001b[1m");
            if (underline) sb.Append("\u001b[4m");
            if (reversed) sb.Append("\u001b[7m");
            return sb.ToString();
        }

        public string getDifference(Formatting previousFormatting)
        {
            StringBuilder sb = new StringBuilder();
            this.getDifference(previousFormatting, sb);
            return sb.ToString();
        }

        public void getDifference(Formatting previousFormatting, StringBuilder sb)
        {
            if (previousFormatting == null || previousFormatting.bold || previousFormatting.underline || previousFormatting.reversed)
            {
                sb.Append(Formatting.RESET_STR).Append(this.ToString());
                return;
            }

            if (!this.color.isEqualTo(previousFormatting.color))
            {
                sb.Append(this.color.getCode());
            }
            if (!this.background.isEqualTo(previousFormatting.background))
            {
                sb.Append(this.background.getBackgroundCode());
            }
        }

        public override string ToString()
        {
            return this.getANSI();
        }

        public Formatting setBold(bool bold)
        {
            this.bold = bold;
            return this;
        }

        public Formatting setUnderline(bool underline)
        {
            this.underline = underline;
            return this;
        }

        public Formatting setReversed(bool reversed)
        {
            this.reversed = reversed;
            return this;
        }

        public Formatting setColor(Color color)
        {
            this.color = color;
            return this;
        }

        public Formatting setBackground(Color background)
        {
            this.background = background;
            return this;
        }

        private Formatting setReset()
        {
            this.isReset = true;
            return this;
        }


        public Formatting() : this(Color.RESET) { }

        public Formatting(Color color)
        {
            this.color = color;
            this.background = Color.RESET;
            this.bold = false;
            this.underline = false;
            this.reversed = false;
        }

        public Formatting(Formatting f)
        {
            this.color = f.color;
            this.background = f.background;
            this.bold = f.bold;
            this.underline = f.underline;
            this.reversed = f.reversed;
        }


        public bool isEqualTo(Formatting f)
        {
            if (f == null) return false;
            return this.isReset == f.isReset
            && this.color.isEqualTo(f.color)
            && this.background.isEqualTo(f.background)
            && this.bold == f.bold
            && this.underline == f.underline
            && this.reversed == f.reversed;
        }





        public static readonly Formatting RESET = new Formatting().setReset();
        public static readonly string RESET_STR = Formatting.RESET.ToString();

        public static readonly Formatting BLACK = new Formatting().setColor(Color.BLACK);
        public static readonly Formatting RED = new Formatting().setColor(Color.RED);
        public static readonly Formatting GREEN = new Formatting().setColor(Color.GREEN);
        public static readonly Formatting YELLOW = new Formatting().setColor(Color.YELLOW);
        public static readonly Formatting BLUE = new Formatting().setColor(Color.BLUE);
        public static readonly Formatting MAGENTA = new Formatting().setColor(Color.MAGENTA);
        public static readonly Formatting CYAN = new Formatting().setColor(Color.CYAN);
        public static readonly Formatting WHITE = new Formatting().setColor(Color.WHITE);

        public static readonly Formatting BRIGHT_BLACK = new Formatting().setColor(Color.BRIGHT_BLACK);
        public static readonly Formatting BRIGHT_RED = new Formatting().setColor(Color.BRIGHT_RED);
        public static readonly Formatting BRIGHT_GREEN = new Formatting().setColor(Color.BRIGHT_GREEN);
        public static readonly Formatting BRIGHT_YELLOW = new Formatting().setColor(Color.BRIGHT_YELLOW);
        public static readonly Formatting BRIGHT_BLUE = new Formatting().setColor(Color.BRIGHT_BLUE);
        public static readonly Formatting BRIGHT_MAGENTA = new Formatting().setColor(Color.BRIGHT_MAGENTA);
        public static readonly Formatting BRIGHT_CYAN = new Formatting().setColor(Color.BRIGHT_CYAN);
        public static readonly Formatting BRIGHT_WHITE = new Formatting().setColor(Color.BRIGHT_WHITE);

        public static readonly Formatting BOLD = new Formatting().setBold(true);
        public static readonly Formatting UNDERLINE = new Formatting().setUnderline(true);
        public static readonly Formatting REVERSED = new Formatting().setReversed(true);


        public static Formatting fromName(string formattingName)
        {
            string name = formattingName.ToLower();
            switch (name)
            {
                case "reset": return Formatting.RESET;
                case "bold": return Formatting.BOLD;
                case "underline": return Formatting.UNDERLINE;
                case "reversed": return Formatting.REVERSED;

                default: return new Formatting(Color.fromName(formattingName));
            }
        }
    }
}