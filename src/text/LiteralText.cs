namespace cscsg
{
    class LiteralText : FormattedText
    {
        private string text;

        public override string getUnformatted()
        {
            return this.text;
        }

        public LiteralText(string text)
        {
            this.text = text;
        }
    }
}