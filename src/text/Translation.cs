using System.Collections.Generic;
using System.Text.Json;
using System.IO;
using System;

namespace cscsg
{
    class Translation
    {

        static Dictionary<string, string> translations = new Dictionary<string, string>();

        static string translationName = "";

        public static void clear()
        {
            translations.Clear();
        }

        public static bool loadFile(string filename)
        {
            try
            {
                string jsonString = File.ReadAllText(filename);
                JsonDocument d = JsonDocument.Parse(jsonString);
                JsonElement r = d.RootElement;

                foreach (var x in r.EnumerateObject())
                {
                    translations[x.Name] = x.Value.GetString();
                }
                return true;
            }
            catch (Exception e)
            {
                Game.error(e.ToString());
                return false;
            }
        }

        public static void reload(string translationName = null)
        {
            if (translationName == null)
            {
                translationName = Translation.translationName;
            }
            
            Translation.clear();
            Translation.loadFile("res/lang/en_us.json");
            if (translationName != "" && translationName != "en_us")
            {
                Translation.loadFile($"res/lang/{translationName}.json");
            }

            Translation.translationName = translationName;
        }

        public static string get(string key)
        {
            return translations.GetValueOrDefault(key, $"MISSING::{key}");
        }

        public static bool has(string key)
        {
            return translations.ContainsKey(key);
        }
    }
}