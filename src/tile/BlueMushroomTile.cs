namespace cscsg
{
    class BlueMushroomTile : PlantTile
    {

        public override bool use(World world, Vector3<int> pos, Player player, ItemStack stack)
        {
            if (player.mana >= player.getMaxMana())
            {
                return false;
            }
            stack.count--;
            player.addMana(20);
            return true;
        }

        public BlueMushroomTile(byte id) : base(id,
            new TileChar('^', new Formatting(Color.BLUE)),
            TC_AIR
        )
        {
            this.setSolid(false);
            this.setName("tile.blueMushroom");
        }
    }
}