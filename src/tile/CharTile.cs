namespace cscsg
{
    class CharTile : Tile
    {
        TileChar wallChar;
        TileChar floorChar;

        public override TileChar getWallChar(World world, int x, int y, int z)
        {
            return this.wallChar;
        }

        public override TileChar getFloorChar(World world, int x, int y, int z)
        {
            return this.floorChar;
        }

        public CharTile(byte id, char c)
            : base(id)
        {
            this.wallChar = new TileChar(c, Formatting.RESET);
            this.floorChar = new TileChar(c, Formatting.RESET);
        }

        public CharTile(byte id, TileChar wallChar, TileChar floorChar)
            : base(id)
        {
            this.wallChar = wallChar;
            this.floorChar = floorChar;
        }
    }
}