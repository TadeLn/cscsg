namespace cscsg
{
    class SaplingTile : TransparentTile
    {
        public override void randomTick(World world, int x, int y, int z)
        {
            world.setTile(x, y, z, Tile.TREE);
        }

        public SaplingTile(byte id) : base(id,
            new TileChar('o', new Formatting(Color.fromRGB(0x10, 0x80, 0x10))),
            TC_AIR
        )
        {
            this.setSolid(false);
            this.setName("tile.sapling");
        }
    }
}