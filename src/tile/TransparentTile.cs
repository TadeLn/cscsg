namespace cscsg
{
    class TransparentTile : CharTile
    {
        public override TileChar getWallChar(World world, int x, int y, int z)
        {
            TileChar t = base.getWallChar(world, x, y, z);
            t.format.background = world.getTile(x, y - 1, z).getFloorChar(world, x, y - 1, z).format.background;
            return t;
        }

        public TransparentTile(byte id, char c)
            : base(id, c)
        {
        }

        public TransparentTile(byte id, TileChar wallChar, TileChar floorChar)
            : base(id, wallChar, floorChar)
        {
        }
    }
}