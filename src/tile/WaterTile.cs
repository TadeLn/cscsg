namespace cscsg
{
    class WaterTile : Tile
    {
        bool shallow;

        static TileChar tile1 = new TileChar('.', new Formatting(Color.WHITE).setBackground(Color.BLUE));
        static TileChar tile2 = new TileChar('~', new Formatting(Color.WHITE).setBackground(Color.BLUE));
        static TileChar tile1s = new TileChar('.', new Formatting(Color.WHITE).setBackground(Color.CYAN));
        static TileChar tile2s = new TileChar('~', new Formatting(Color.WHITE).setBackground(Color.CYAN));

        public override TileChar getFloorChar(World world, int x, int y, int z)
        {
            if (this.shallow)
            {
                return Game.random.NextDouble() < 0.8 ? tile1s : tile2s;
            }
            else
            {
                return Game.random.NextDouble() < 0.5 ? tile1 : tile2;
            }
        }

        public override TileChar getWallChar(World world, int x, int y, int z)
        {
            return getFloorChar(world, x, y, z);
        }

        public override bool canBeStoodOn()
        {
            return shallow;
        }

        public override bool isSolid()
        {
            return false;
        }

        public override bool isUnbreakable()
        {
            return true;
        }


        public override void entityCollision(World world, Vector3<int> pos, Entity entity)
        {
            if (this.shallow)
            {
                entity.move(world, Direction.UP);
            }
        }


        public WaterTile(byte id, bool shallow = false) : base(id)
        {
            this.shallow = shallow;
        }
    }
}