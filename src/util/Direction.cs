namespace cscsg
{
    enum Direction
    {
        INVALID,
        NORTH,
        SOUTH,
        EAST,
        WEST,
        UP,
        DOWN
    }

    static class DirectionExtensions
    {
        public static Direction invert(this Direction that)
        {
            switch (that)
            {
                case Direction.NORTH:
                    return Direction.SOUTH;

                case Direction.SOUTH:
                    return Direction.NORTH;

                case Direction.EAST:
                    return Direction.WEST;

                case Direction.WEST:
                    return Direction.EAST;

                case Direction.UP:
                    return Direction.DOWN;

                case Direction.DOWN:
                    return Direction.UP;

                default:
                    return Direction.INVALID;
            }
        }
    }
}
