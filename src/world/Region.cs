using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Text.Json.Nodes;

namespace cscsg
{
    class Region : JsonSerializable
    {
        public const int REGION_SIZE = 32;

        public Dictionary<long, Chunk> chunks = new Dictionary<long, Chunk>();

        public static Region load(int regionX, int regionZ, string worldName)
        {
            Region result = new Region();
            string path = Path.Join("world", worldName, "region", $"r_{regionX}_{regionZ}.json.gz");
            if (!File.Exists(path))
            {
                return null;
            }
            try
            {
                JsonNode node = JsonLoader.loadCompressedFileE(path);
                if (node == null)
                {
                    throw new Exception();
                }

                if (!result.loadJson(node.AsObject()))
                {
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                Game.error(new TranslatableText("error.whileLoadingFile", path)
                    .append("\n")
                    .append(e.ToString())
                );
                return null;
            }
            return result;
        }

        public void save(int regionX, int regionZ, string worldName)
        {
            string dirpath = Path.Join("world", worldName, "region");
            Directory.CreateDirectory(dirpath);
            string path = Path.Join(dirpath, $"r_{regionX}_{regionZ}.json.gz");

            JsonLoader.saveCompressedFile(path, this.toJson());
        }





        public static Region fromJson(JsonObject j)
        {
            Region result = new Region();
            if (!result.loadJson(j)) return null;
            return result;
        }

        public bool loadJson(JsonObject j)
        {
            int version = -1;
            JsonHelper.load(j, "version", ref version);
            if (version != 1)
            {
                Game.error(new TranslatableText("error.world.load.wrongVersion"));
                return false;
            }

            Dictionary<string, JsonObject> chunks = JsonHelper.loadObjDict(j, "chunks");
            foreach (var it in chunks)
            {
                try
                {
                    this.chunks.Add(Convert.ToInt64(it.Key), Chunk.fromJson(it.Value));
                }
                catch (System.Exception) { }
            }
            return true;
        }

        public virtual JsonObject toJson()
        {
            JsonObject j = new JsonObject();

            JsonObject chunks = new JsonObject();
            foreach (var it in this.chunks)
            {
                try
                {
                    chunks.Add(it.Key.ToString(), it.Value.toJson());
                }
                catch (Exception e)
                {
                    Game.error(new TranslatableText("error.world.save.chunk")
                        .append(" ")
                        .append(e.ToString())
                    );
                }
            }
            j.Add("version", JsonValue.Create<int>(1));
            j.Add("chunks", chunks);
            return j;
        }

    }
}