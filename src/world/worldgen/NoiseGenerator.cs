using System;
using System.Collections.Generic;

namespace cscsg
{
    class NoiseGenerator : WorldGenerator
    {
        int seed = 0;
        Dictionary<long, Vector2<float>> vectors = new Dictionary<long, Vector2<float>>();

        static float interpolate(float a0, float a1, float w)
        {
            return (a1 - a0) * w + a0;
        }

        Vector2<float> randomGradient(int x, int y)
        {
            long id = World.toCoordsId(x, y);
            if (!vectors.ContainsKey(id))
            {
                Random random = new Random(World.getChunkSeed(this.seed, x, y));
                random.NextDouble();
                random.NextDouble();
                double r = random.NextDouble();
                vectors[id] = new Vector2<float>((float)Math.Sin(r), (float)Math.Cos(r));
            }
            return vectors[id];
        }

        float dotGridGradient(int ix, int iy, float x, float y)
        {
            Vector2<float> gradient = randomGradient(ix, iy);

            // Compute the distance vector
            float dx = x - (float)ix;
            float dy = y - (float)iy;

            // Compute the dot-product
            return (dx * gradient.x + dy * gradient.y);
        }

        public float getPerlin(float x, float y)
        {
            // Determine grid cell coordinates
            int x0 = (int)Math.Floor(x);
            int x1 = x0 + 1;
            int y0 = (int)Math.Floor(y);
            int y1 = y0 + 1;

            // Determine interpolation weights
            // Could also use higher order polynomial/s-curve here
            float sx = x - (float)x0;
            float sy = y - (float)y0;

            // Interpolate between grid point gradients
            float n0, n1, ix0, ix1, value;

            n0 = dotGridGradient(x0, y0, x, y);
            n1 = dotGridGradient(x1, y0, x, y);
            ix0 = interpolate(n0, n1, sx);

            n0 = dotGridGradient(x0, y1, x, y);
            n1 = dotGridGradient(x1, y1, x, y);
            ix1 = interpolate(n0, n1, sx);

            value = interpolate(ix0, ix1, sy);
            return value;
            // [TODO] use a better perlin noise function
        }

        public static byte getTileFromPerlin(float perlin) {
            if (perlin < -0.1)
            {
                return Tile.WATER;
            }
            else if (perlin < 0)
            {
                return Tile.SHALLOW_WATER;
            }
            else if (perlin < 0.1)
            {
                return Tile.SAND;
            }
            else if (perlin < 0.35)
            {
                return Tile.GRASS;
            }
            else
            {
                return Tile.FOREST_GRASS;
            }
        }

        public override void generateChunkShape(World world, int chunkX, int chunkZ)
        {
            this.seed = world.getSeed();

            Chunk c = new Chunk();
            int chunkStartPosX = chunkX * Chunk.CHUNK_SIZE;
            int chunkStartPosZ = chunkZ * Chunk.CHUNK_SIZE;

            const float SCALE_X = 0.015f;
            const float SCALE_Z = 0.015f;
            const float SCALE_PERLIN = 5f;

            for (int zOff = 0; zOff < Chunk.CHUNK_SIZE; zOff++)
            {
                int z = chunkStartPosZ + zOff;
                for (int xOff = 0; xOff < Chunk.CHUNK_SIZE; xOff++)
                {
                    int x = chunkStartPosX + xOff;

                    c.setTile(xOff, 0, zOff, Tile.BEDROCK);
                    for (int y = 1; y < World.SURFACE_LEVEL - 1; y++)
                    {
                        c.setTile(xOff, y, zOff, Tile.STONE);
                    }
                    c.setTile(xOff, World.SURFACE_LEVEL - 1, zOff, Tile.DIRT);

                    float result = this.getPerlin(((float)x) * SCALE_X, ((float)z) * SCALE_Z) * SCALE_PERLIN;
                    c.setTile(xOff, World.SURFACE_LEVEL, zOff, NoiseGenerator.getTileFromPerlin(result));
                }
            }
            world.setChunk(chunkX, chunkZ, c);
        }

        public NoiseGenerator()
        {
        }
    }
}