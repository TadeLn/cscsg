using System;
using System.Collections.Generic;

namespace cscsg
{
    abstract class WorldGenerator
    {
        public abstract void generateChunkShape(World world, int chunkX, int chunkZ);
        public virtual void populateChunk(World world, int chunkX, int chunkZ)
        {
            Random random = new Random(world.getChunkSeed((short)chunkX, (short)chunkZ));

            foreach (Decorator decorator in this.decorators) {
                decorator.decorate(world, chunkX, chunkZ, random);
            }
            world.getChunk(chunkX, chunkZ).isPopulated = true;
        }
        public virtual void fullGenerateChunk(World world, int chunkX, int chunkZ)
        {
            this.generateChunkShape(world, chunkX, chunkZ);
            this.populateChunk(world, chunkX, chunkZ);
        }

        public List<Decorator> decorators = new List<Decorator>();

        public WorldGenerator addDecorator(Decorator decorator)
        {
            this.decorators.Add(decorator);
            return this;
        }
    }
}