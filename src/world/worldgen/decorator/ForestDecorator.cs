namespace cscsg
{
    class ForestDecorator : PlaneDecorator
    {
        public ForestDecorator() : base(0.1f, Tile.TREE, Tile.FOREST_GRASS) { }
    }
}