namespace cscsg
{
    class TreeDecorator : PatchDecorator
    {
        public TreeDecorator() : base(3, 7, 0.1f, 0.01f, Tile.TREE, Tile.GRASS, 0.3f) { }
    }
}